cmake_minimum_required(VERSION 3.17 )

project(stomt-sdk-example VERSION 1.0)


add_subdirectory(sdk)
add_subdirectory(example)
