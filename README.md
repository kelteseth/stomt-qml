# STOMT stomt-qt-sdk [![STOMT API](https://img.shields.io/badge/stomt-v2.10.X-brightgreen.png)](https://rest.stomt.com/)

This is an unofficial Qt/QML integration. [Give us (anonymous) feedback via stomtQtSDK @stomt!](https://www.stomt.com/stomtqtsdk)

<div align="center">
  <a href="https://www.stomt.com/stomtqtsdk">
    <img alt="STOMT Qt/QML feedback integration" src="https://i.imgur.com/h1GBFLg.png" />
  </a>
</div>

## Why?

STOMT allows you to collect simple and constructive feedback in-game and [also from your website](https://stomt.co/web) and collect it at one central place where you manage it and where your community can vote and comment on the feedback of others.

## Used by
* [ScreenPlay](https://screen-play.app/) - Modern, Cross Plattform, Live Wallpaper, Widgets and AppDrawer!
* Your app here!

## Requirements
* Minimum Qt version: Qt 5.9
* Tested Qt version: Qt 5.10 -> Qt 5.15
* Dependencies via Qt Maintainance tool:
    * Qt > 5.9
    * cmake
    * OpenSSL 

## Installation

1. Clone this repository
2. Open the CMakeLists.txt in QtCreator
3. Add install to the build steps. Projects -> Build -> Add Build Step -> Select Build -> Select "install" as target
4. Press build. This will compile the project and copy all necessary files into your Qt installation 
 * Example Path: C:\Qt\5.14.2\msvc2017_64\qml\com\stomt\sdk
5. Add OpenSSL from C:\Qt\Tools\OpenSSL\Win_x64\bin into your .exe dir
    * libcrypto-1_1-x64.dll
    * libssl-1_1-x64.dll
 
 ## Usage
```qml
import com.stomt.qml 1.0

    StomtWidget {
        targetID: "screenplay"
        appKey: "YourAppID"
        targetDisplayName: "ScreenPlay"
        targetImageUrl: "qrc:/assets/icons/favicon.ico"
    }
    
```
## Use our Sandbox

If you want to test the integration please feel free to do what you want on [test.stomt.com](https://test.stomt.com/)

* Just go through the configuration steps again using the test server:

1. Register on [test.stomt.com](https://test.stomt.com/signup/game).
2. And create an [App Id](https://test.stomt.com/integrate) for your project.
3. Add this to your StomtWidget {...}

```qml
        useTestServer: true
```

## Contribution

We would love to see you contributing to this project. Feel free to fork it and send in your pull requests! Visit the [project on STOMT](https://www.stomt.com/stomtqtsdk) to support with your ideas, wishes and feedback.


## Authors

[Elias Steurer](https://gitlab.com/kelteseth) | [Follow me on STOMT](https://www.stomt.com/kelteseth)


## More about stomt

* On the web [www.stomt.com](https://www.stomt.com)
* [STOMT for iOS](http://stomt.co/ios)
* [STOMT for Android](http://stomt.co/android)
* [STOMT for Unity](http://stomt.co/unity)
* [STOMT for Websites](http://stomt.co/web)
* [STOMT for Wordpress](http://stomt.co/wordpress)
* [STOMT for Drupal](http://stomt.co/drupal)
